/*
Folder and Files Preparation: 

1. Create a "readingListAct" Folder in your Batch Folder. 
2. Create an index.html and index.js file. For the index.html title just write "Reading List Activity 1" 
3. Show your solutions for each problem in the index.js file. 
4. Once done, Create a remote repository named "readingListAct".
5. Save your repo link in Boodle: WDC028V1.5b-18-C1 | JavaScript - Function Parameters, Return Statement and Array Manipulations


*/

// Activity Template:
// (Copy this part on your template)

/*
	1.) Create a function that returns a passed string with letters in alphabetical order.
		Example string: 'mastermind'
		Expected output: 'adeimmnrst'
	
*/

// Code here:
const sortLetters = (str) => {
	return str.split('').sort().join('')
}

console.log(sortLetters('mastermind'))

/*
	2.) Write a simple JavaScript program to join all elements of the following array into a string.

	Sample array : myColor = ["Red", "Green", "White", "Black"];
		Expected output: 
			Red,Green,White,Black
			Red,Green,White,Black
			Red+Green+White+Black

*/

// Code here:
myColor = ['Red', 'Green', 'White', 'Black']

console.log(`${myColor[0]},${myColor[1]},${myColor[2]},${myColor[3]}`)
console.log(`${myColor[0]},${myColor[1]},${myColor[2]},${myColor[3]}`)
console.log(`${myColor[0]}+${myColor[1]}+${myColor[2]}+${myColor[3]}`)

/*
	3.) Write a function named birthdayGift that pass a gift as an argument.
		- if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
		- if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
		- if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
		- if other gifts return a message that says: "Thank you for the (gift), Dad!"
		- create a global variable named myGift and invoke the function in the variable.
		- log the global var in the console

*/

// Code here:
const birthdayGift = (gift) => {
	const birthdayGift = gift.toLowerCase()
	let message = `Thank you for the ${birthdayGift}, `

	switch (birthdayGift) {
		case 'stuffed toy':
			message += 'Michael!'
			break
		case 'doll':
			message += 'Sarah!'
			break
		case 'cake':
			message += 'Donna'
			break
		default:
			message += 'Dad!'
			break
	}

	return message
}

const myGift = birthdayGift('Stuffed toy')
console.log(myGift)

/*
	4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.

		Example string: 'The quick brown fox'
		Expected Output: 5

*/

// Code here:

const countVowels = (str) => {
	const vowels = ['a', 'e', 'i', 'o', 'u']
	let counter = 0

	str.toLowerCase()
		.split('')
		.forEach((letter) => {
			if (vowels.includes(letter)) {
				counter++
			}
		})

	return counter
}

console.log(countVowels('The quick brown fox'))
